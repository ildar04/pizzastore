﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PizzaStoreLib.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PizzaStoreLib.Authentication
{
    public class AuthenticationVk
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        public AuthenticationVk(IConfiguration configuration, ILogger<AuthenticationVk> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<AccessTokenVk> GetAccessToken(string code)
        {
            try
            {
                string query = _configuration["VKAuthentication:access_token_uri"] +
                            "?client_id=" + _configuration["VKAuthentication:client_id"] +
                            "&client_secret=" + _configuration["VKAuthentication:client_secret"] +
                            "&redirect_uri=" + _configuration["VKAuthentication:redirect_uri"] +
                            "&code=" + code;

                using (var webClient = new WebClient())
                {
                    var responseText = await webClient.DownloadStringTaskAsync(query);
                    var model = JsonConvert.DeserializeObject<AccessTokenVk>(responseText);
                    return model;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return new AccessTokenVk();
            }
        }

        public async Task<UserInfoVk> GetUserInfo(int id)
        {
            try
            {
                string url = _configuration["VKAuthentication:user_info_uri"] +
                            "?user_ids=" + id.ToString() +
                            _configuration["VKAuthentication:version_parameter"] +
                            _configuration["VKAuthentication:access_token_parameter"];

                using (var webClient = new WebClient())
                {
                    var responseText = await webClient.DownloadStringTaskAsync(url);
                    var model = JsonConvert.DeserializeObject<ResponseObjectVk<UserInfoVk>>(responseText);
                    var modelResults = model.Results;
                    var userInfo = modelResults.Count > 0
                        ? model.Results[0]
                        : new UserInfoVk();
                    return userInfo;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return new UserInfoVk();
            }
        }

        public string GetVkAuthenticationURL()
        {
            return "https://oauth.vk.com/authorize" +
                    "?client_id=" + _configuration["VKAuthentication:client_id"] +
                    "&display=page" +
                    "&redirect_uri=" + _configuration["VKAuthentication:redirect_uri"] +
                    "&scope=1" +
                    "&response_type=code" +
                    _configuration["VKAuthentication:version_parameter"];
        }
    }
}
