﻿using Newtonsoft.Json;

namespace PizzaStoreLib.Models
{
    public class AccessTokenVk
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; } = "";
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("error")]
        public string ErrorTitle { get; set; } = "";
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; } = "";
    }
}
