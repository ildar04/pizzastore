﻿using Newtonsoft.Json;

namespace PizzaStoreLib.Models
{
    public class UserInfoVk
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; } = "";
        [JsonProperty("last_name")]
        public string LastName { get; set; } = "";
    }
}
