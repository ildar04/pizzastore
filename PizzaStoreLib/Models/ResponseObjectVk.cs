﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PizzaStoreLib.Models
{
    public class ResponseObjectVk<T>
    {
        [JsonProperty("response")]
        public List<T> Results { get; set; }
    }
}
