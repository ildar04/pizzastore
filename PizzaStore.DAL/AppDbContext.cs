﻿using Microsoft.EntityFrameworkCore;
using PizzaStore.DAL.Entities;

namespace PizzaStore.DAL
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(string connectionString)
            : base(new DbContextOptionsBuilder<AppDbContext>().UseSqlServer(connectionString).Options)
        { }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<PizzaEntity> Pizzas { get; set; }
        public DbSet<IngredientEntity> Ingredients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Many_to_many
            
            modelBuilder.Entity<PizzaIngredient>()
                .HasKey(pi => new { pi.PizzaId, pi.IngredientId });

            modelBuilder.Entity<PizzaIngredient>()
                .HasOne(pi => pi.Pizza)
                .WithMany(b => b.PizzaIngredients)
                .HasForeignKey(bc => bc.PizzaId);

            modelBuilder.Entity<PizzaIngredient>()
                .HasOne(pi => pi.Ingredient)
                .WithMany(c => c.PizzaIngredients)
                .HasForeignKey(bc => bc.IngredientId);
            #endregion
        }
    }
}
