﻿namespace PizzaStore.DAL.Entities
{
    public class PizzaIngredient
    {
        public int PizzaId { get; set; }
        public PizzaEntity Pizza { get; set; }
        public int IngredientId { get; set; }
        public IngredientEntity Ingredient { get; set; }
    }
}
