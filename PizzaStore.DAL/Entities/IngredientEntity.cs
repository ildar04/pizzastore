﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PizzaStore.DAL.Entities
{
    public class IngredientEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual ICollection<PizzaIngredient> PizzaIngredients { get; set; }
    }
}
