﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using PizzaStore.Controllers;
using Xunit;

namespace PizzaStore.Tests.PizzaStore
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexViewDataMessage()
        {
            // Arrange
            var mock = new Mock<ILogger<HomeController>>();
            //mock.CallBase = true;
            //mock.Setup(x => x.ToString()).Returns("asdas");
            //var str = mock.Object.ToString();

            var controller = new HomeController(mock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.Equal("Hello world!", result?.ViewData["Message"]);
        }

        [Fact]
        public void IndexViewResultNotNull()
        {
            // Arrange
            var mock = new Mock<ILogger<HomeController>>();
            var controller = new HomeController(mock.Object);
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void IndexViewNameEqualIndex()
        {
            // Arrange
            var mock = new Mock<ILogger<HomeController>>();
            var controller = new HomeController(mock.Object);            
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.Equal("Index", result?.ViewName);
        }
    }
}
