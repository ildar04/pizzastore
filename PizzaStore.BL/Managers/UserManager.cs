﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PizzaStore.BL.Models;
using PizzaStore.DAL.Entities;
using System.Threading.Tasks;

namespace PizzaStore.BL.Managers
{
    public class UserManager : BaseManager
    {
        public UserManager(IConfiguration configuration, IMapper mapper) : base(configuration, mapper)
        { }

        public async Task<bool> UserExists(string login)
        {
            using (var db = DbContext())
            {
                return await db.Users.AnyAsync(u => u.Login == login);
            }
        }

        public async Task<int> AddUser(User newUser)
        {
            using (var db = DbContext())
            {
                var newUserEntity = new UserEntity();
                _mapper.Map(newUser, newUserEntity);
                db.Users.Add(newUserEntity);
                return await db.SaveChangesAsync();
            }
        }

        public async Task<User> GetUser(string login)
        {
            using (var db = DbContext())
            {
                var userEntity = await db.Users.SingleOrDefaultAsync(u => u.Login == login);

                if (userEntity == null)
                    return null;

                var user = new User();
                _mapper.Map(userEntity, user);
                return user;
            }
        }
    }
}
