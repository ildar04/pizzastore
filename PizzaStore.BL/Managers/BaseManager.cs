﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using PizzaStore.DAL;

namespace PizzaStore.BL.Managers
{
    public class BaseManager
    {
        protected readonly string _connectionString;
        protected readonly IMapper _mapper;

        public BaseManager(IConfiguration configuration, IMapper mapper)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _mapper = mapper;
        }

        protected AppDbContext DbContext() => new AppDbContext(_connectionString);
    }
}
