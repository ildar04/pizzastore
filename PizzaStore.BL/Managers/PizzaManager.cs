﻿using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace PizzaStore.BL.Managers
{
    public class PizzaManager : BaseManager
    {

        public PizzaManager(IConfiguration configuration, IMapper mapper) : base(configuration, mapper)
        { }


    }
}
