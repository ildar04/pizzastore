﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PizzaStore.Models;

namespace PizzaStore.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            try
            {
                ViewData["Message"] = "Hello world!";
                _logger.LogDebug(ViewData["Message"].ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return View("Index");
        }

        public IActionResult About()
        {
            try
            {
                ViewData["Message"] = "Your application description page.";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return View();
        }

        public IActionResult Contact()
        {
            try
            {
                ViewData["Message"] = "Your contact page.";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            return View();
        }

        public IActionResult Error()
        {
            // Get the details of the exception that occurred
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                // Get which route the exception occurred at
                string routeWhereExceptionOccurred = exceptionFeature.Path;

                // Get the exception that occurred
                Exception exceptionThatOccurred = exceptionFeature.Error;

                // TODO: Do something with the exception
                // Log it with Serilog?
                // Send an e-mail, text, fax, or carrier pidgeon?  Maybe all of the above?
                // Whatever you do, be careful to catch any exceptions, otherwise you'll end up with a blank page and throwing a 500
                _logger.LogError(exceptionThatOccurred, $"Global error. Route: {routeWhereExceptionOccurred}");
            }

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public ActionResult Test()
        {
            return View((object)"Hello, world!");
        }

        [HttpPost]
        public ActionResult Test(string text)
        {
            //HttpContext.Response.Headers.Add("X-XSS-Protection", "0");
            return View((object)text);
        }
    }
}
