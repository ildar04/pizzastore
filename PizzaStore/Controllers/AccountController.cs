﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PizzaStore.BL.Managers;
using PizzaStore.BL.Models;
using PizzaStore.BL.ViewModels;
using PizzaStoreLib.Authentication;

namespace PizzaStore.Controllers
{
    public class AccountController : Controller
    {
        private const string AuthenticationTypeVK = "VK"; //TODO: Вывести куда-нибудь.

        private readonly UserManager _userManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly AuthenticationVk _vKAuth;

        public AccountController(UserManager userManager, ILogger<AccountController> logger, IConfiguration configuration, AuthenticationVk vKAuth)
        {
            _userManager = userManager;
            _logger = logger;
            _configuration = configuration;
            _vKAuth = vKAuth;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            ViewBag.VkAuthenticationURL = _vKAuth.GetVkAuthenticationURL();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                ViewBag.VkAuthenticationURL = _vKAuth.GetVkAuthenticationURL();

                if (ModelState.IsValid)
                {
                    User user = await _userManager.GetUser(model.Email);
                    if (user == null)
                    {
                        ModelState.AddModelError("Email", $"Пользователя \"{model.Email}\" не существует.");
                        return View(model);
                    }

                    if (!string.IsNullOrEmpty(user.AuthenticationType))
                    {
                        if(user.AuthenticationType == AuthenticationTypeVK)
                            ModelState.AddModelError("Email", "Ваш аккаунт настроен на авторизацию через ВКонтакте");
                        // Предполагается, что в будущем будет использовано больше сервисов авторизации, например:
                        //if (user.AuthenticationType == AuthenticationTypeFacebook)
                        //    ModelState.AddModelError("Email", "Ваш аккаунт настроен на авторизацию через Facebook");

                        return View(model);
                    }

                    string hashedInputPassword = GetHashedString(model.Password, user.Salt);
                    if (user.Password == hashedInputPassword)
                    {
                        await Authenticate(model.Email);

                        return RedirectToAction("Index", "Home");
                    }
                    ModelState.AddModelError("Password", "Некорректный пароль");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return Content(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> VKAuthentication(string code, string error, string error_description)
        {
            try
            {
                if (String.IsNullOrEmpty(code))
                {
                    var errorMessage = $"VK Authentication: error={error}, error_description={error_description}";
                    _logger.LogError(errorMessage);
                    return Content(errorMessage);
                }

                var accessTocken = await _vKAuth.GetAccessToken(code);

                if (accessTocken.UserId != 0)
                {
                    var isUserExists = await _userManager.UserExists(accessTocken.UserId.ToString());
                    if(isUserExists)
                    {
                        var userInfo = await _vKAuth.GetUserInfo(accessTocken.UserId);
                        var newUser = new User
                        {
                            Login = accessTocken.UserId.ToString(),
                            // Не может быть, что userInfo.FirstName или userInfo.LastName равны NullOrEmpty, так как 
                            // их поиск осуществляется по id полученному из запроса аутентификации.
                            // Кроме случев если: сервер VK упадет. Либо если что то произойдет с запросом со стороны
                            // приложения (упал интернет или неправильный код)
                            FirstName = userInfo.FirstName,
                            LastName = userInfo.LastName,
                            AuthenticationType = AuthenticationTypeVK
                        };
                        await _userManager.AddUser(newUser);
                    }

                    await Authenticate(accessTocken.UserId.ToString());
                    return RedirectToAction("Index", "Home");
                }
                else if (!String.IsNullOrEmpty(accessTocken.ErrorTitle))
                {
                    string errorMessage = $"AccessTocken: error={accessTocken.ErrorTitle}, error_description={accessTocken.ErrorDescription}";
                    _logger.LogError(errorMessage);
                    return Content(errorMessage);
                }

                return Content($"Something wrong with getting access token. Contact the administrator at ivazetdinov@htc-cs.com ");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return Content(ex.Message);
            }
        }
        
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var isUserExists = await _userManager.UserExists(model.Email);
                    if (!isUserExists)
                    {
                        string salt = GenSalt(32);
                        string hashedPassword = GetHashedString(model.Password, salt);

                        var newUser = new User {
                            Login = model.Email,
                            Email = model.Email,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Password = hashedPassword,
                            Salt = salt
                        };

                        await _userManager.AddUser(newUser);

                        await Authenticate(newUser.Login);

                        return RedirectToAction("Index", "Home");
                    }
                    else
                        ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return Content(ex.Message);
            }
        }

        private string GenSalt(int length)
        {
            RNGCryptoServiceProvider p = new RNGCryptoServiceProvider();
            var salt = new byte[length];
            p.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        private string GetHashedString(string value, string salt)
        {
            SHA256 shaManager = new SHA256Managed();
            byte[] hashResult = shaManager.ComputeHash(Encoding.ASCII.GetBytes(value + salt));
            return Encoding.ASCII.GetString(hashResult);
        }

        private async Task Authenticate(string userName)
        {
            try
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
                };
                ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                // Установка аутентификационных куки.
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
            }
        }

        public async Task<IActionResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                return Content(ex.Message);
            }
        }
    }
}